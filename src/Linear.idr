module Linear

import Category

infixr 0 #->

public export
data (#->) : Type -> Type -> Type where
  Lin : ((1 _ : a) -> b) -> a #-> b

||| Function Application
public export
(|>) : forall a, b. a #-> b -> (1 _ : a) -> b
(|>) (Lin f) v = f v

||| Linear identity
public export
lid : forall a. a #-> a
lid = Lin \x => x

||| linear compsition
public export
lcomp : forall a, b, c. a #-> b -> b #-> c -> a #-> c
lcomp (Lin f) (Lin g) = Lin (\x => g (f x))

||| Functional extentionality for linear functions
0 LinExt : forall a, b. {0 f, g : a #-> b} ->  ((1 v : a) -> f |> v = g |> v) -> f = g

||| Postualte for no duplication of the argument
0 NoDup : a #-> (a, a) -> Void

||| Postulate for no dropping the argument
0 NoDrop : a #-> () -> Void

leftIdentity : (0 v : _) ->  (id v) = v
leftIdentity _ = Refl

0 leftId : (f : a #-> b) -> lcomp (Lin \x => x) f = f
leftId (Lin f) = Refl

0 rightId : (f : a #-> b) -> lcomp f (Lin (\1 x => x)) = f
rightId (Lin f) = Refl

0 keepComp : (f : a #-> b)
          -> (g : b #-> c)
          -> (h : c #-> d)
          -> lcomp f (lcomp g h) = lcomp (lcomp f g) h
keepComp (Lin f) (Lin g) (Lin h) = Refl

public export
LSet : Category Type
LSet = MkCategory (#->)
                  (\t => lid)
                  lcomp
                  leftId
                  rightId
                  keepComp

