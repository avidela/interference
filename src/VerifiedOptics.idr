module ProfunctorOptics

import Data.Vect
import Isomorphism

%hide Num.(+)

data (+) : Type -> Type -> Type where
  L : a -> a + b
  R : b -> a + b

plus : (a -> a') -> (b -> b') -> a + b -> a' + b'
plus l r (L a) = L (l a)
plus l r (R b) = R (r b)

plusElim : (a -> c) -> (b -> c) -> a + b -> c
plusElim f g (L x) = f x
plusElim f g (R x) = g x

pair : (a -> a') -> (b -> b') -> (a, b) -> (a', b')
pair f g (x, y) = (f x, g y)

pair' : Applicative f => (a -> f b) -> (c -> f d) -> (a,c) -> f (b,d)
pair' h g (x, y) = pure MkPair <*> h x <*> g y

lmap : (a -> a') -> (a, b) -> (a', b)
lmap f (x, y) = (f x, y)

fork : (a -> a') -> (a -> b') -> a -> (a', b')
fork f g x = (f x, g x)

record Lens (a, b, s, t : Type) where
  constructor MkLens
  get : s -> a
  set : (b, s) -> t

record Adapter (a, b, s, t : Type) where
  constructor MkAdapter
  from : s -> a
  to : b -> t

record Prism (a, b, s, t : Type) where
  constructor MkPrism
  match : s -> t + a
  build : b -> t

||| Called `the` in the paper
maybePrism : Prism a b (Maybe a) (Maybe b)
maybePrism = MkPrism (maybe (L Nothing) R) Just

-- Apprently this is isomorphic to (n ** (Vect n a, Vect n b -> t))
data FunList : (a, b, t : Type) -> Type where
  Done : t -> FunList a b t
  More : (v : a) -> FunList a b (b -> t) -> FunList a b t

Traversal : (a, b, s, t : Type) -> Type
Traversal a b s t = s -> FunList a b t

interface Profunctor (p : Type -> Type -> Type) where
  dimap : (a' -> a) -> (b -> b') -> p a b -> p a' b'

Arrow : Type -> Type -> Type
Arrow a b = a -> b

implementation Profunctor Arrow where
  dimap f g h = g . h . f

record UpStar (f : Type -> Type) (a, b : Type) where
  constructor Star
  star : a -> f b

Functor f => Profunctor (UpStar f) where
  dimap f g (Star h) = Star $ (map g) . h . f

interface Profunctor p => Cartesian p where
  first : p a b -> p (a, c) (b, c)
  second : p a b -> p (c, a) (c, b)

implementation Cartesian Arrow where
  first h (a, c) = (h a, c)
  second h = map h

Functor f => Cartesian (UpStar f) where
  first (Star star) = Star $ \(a, c) => map (, c) (star a)
  second (Star star) = Star $ \(c, b) =>  map (c,) (star b)


interface Profunctor p => CoCartesian p where
  left : p a b -> p (a + c) (b + c)
  right : p a b -> p (c + a) (c + b)

CoCartesian Arrow where
  left h = plus h id
  right h = plus id h

implementation Applicative f => CoCartesian (UpStar f) where
  left (Star s) = Star $ plusElim (map L . s) (pure . R)
  right (Star s) = Star $ plusElim (pure . L) (map R . s)

interface Profunctor p => Monoidal p where
  par : p a b -> p c d -> p (a, c) (b, d)
  empty : p () ()

Monoidal Arrow where
  par = pair
  empty = id

Applicative f => Monoidal (UpStar f) where
  empty = Star pure
  par (Star h) (Star k) = Star (\(a, c) => [|(h a, k c)|])

Optic : (p : Type -> Type -> Type) -> (a, b, s, t : Type) -> Type
Optic p a b s t = p a b -> p s t

-- Adapters as optics

AdapterP : (a, b, s, t : Type) -> Type
AdapterP a b s t = (0 p : Type -> Type -> Type) -> Profunctor p => Optic p a b s t

implementation Profunctor (Adapter a b) where
  dimap f g (MkAdapter from to) = MkAdapter (from . f) (g . to)

adapterToPro : Adapter a b s t -> AdapterP a b s t
adapterToPro (MkAdapter from to) p = dimap from to

proToAdapter : AdapterP a b s t -> Adapter a b s t
proToAdapter f = f _ (MkAdapter id id)

-- Lenses at optics

LensP : (a, b, s, t : Type) -> Type
LensP a b s t = {0 p : Type -> Type -> Type} -> Cartesian p => Optic p a b s t

implementation Profunctor (Lens a b) where
  dimap f g (MkLens get set) = MkLens (get . f) (g . set . (pair id f))

implementation Cartesian (Lens a b) where
  first (MkLens get set) = MkLens (get . fst) (\(x, (y, z)) =>  (set (x, y), z))
  second (MkLens get set) = MkLens (get . snd) (\(x, (y, z)) => (y, set (x, z)))

lensC2P : Lens a b s t -> LensP a b s t
lensC2P (MkLens get set) = (dimap (fork get id) set) . first

lensP2C : LensP a b s t -> Lens a b s t
lensP2C f = f (MkLens id fst)

PrismP : (a, b, s, t : Type) -> Type
PrismP a b s t = {0 p : Type -> Type -> Type} -> CoCartesian p => Optic p a b s t

implementation Profunctor (Prism a b) where
  dimap f g (MkPrism m b) = MkPrism ((plus g id) . m . f) (g . b)

implementation CoCartesian (Prism a b) where
  left (MkPrism m b) = MkPrism (plusElim ((plus L id) . m) (L . R)) (L . b)
  right (MkPrism m b) = MkPrism (plusElim (L . L) ((plus R id) . m)) (R . b)

prismC2P : Prism a b s t -> PrismP a b s t
prismC2P (MkPrism match build) = 
   (dimap match (plusElim id build)) . right

out : FunList a b t -> t + (a, FunList a b (b -> t))
out (Done x) = L x
out (More v x) = R (v, x)

inn : t + (a, FunList a b (b -> t)) -> FunList a b t
inn (L x) = Done x
inn (R x) = More (fst x) (snd x)

traverse : CoCartesian p => Monoidal p => p a b -> p (FunList a c t) (FunList b c t)
traverse x = dimap out inn (right (par x (traverse x)))


implementation Functor (FunList a b) where
  map f (Done x) = Done (f x)
  map f (More v x) = More v (map (f .) x)

implementation Applicative (FunList a b) where
  pure a = Done a
  (Done x) <*> b = map x b
  (More v x) <*> b = More v (map flip x <*> b)

TraversalP : (a, b, s, t : Type) -> Type
TraversalP a b s t = forall p . Cartesian p => CoCartesian p => Monoidal p => Optic p a b s t

fuse : FunList b b t -> t
fuse (Done y) = y
fuse (More v y) = fuse y v

traversalC2P : Traversal a b s t -> TraversalP a b s t
traversalC2P f x {p} = dimap f fuse (traverse x)

implementation Profunctor (Traversal a b) where
  dimap f g h = map g . h . f

implementation Profunctor (Traversal a b) => Cartesian (Traversal a b) where
  first h (s, c) = map (, c) (h s)
  second h (c, s) = map (c,) (h s)

implementation Profunctor (Traversal a b) => CoCartesian (Traversal a b) where
  left h = plusElim (map L . h) (Done . R)
  right h = plusElim (Done . L) (map R . h)

implementation Profunctor (Traversal a b) => Monoidal (Traversal a b) where
  par h k  = pair' h k
  empty = pure

traverseOf : TraversalP a b s t -> {0 f : Type -> Type} -> Applicative f => (a -> f b) -> s -> f t
traverseOf p g = let ab : UpStar f a b = Star g
                     st : UpStar f s t = p {p = UpStar f} ab in
                     star st

piP1 : Cartesian p => p a b -> p (a, c) (b, c)
piP1 = first

piP11 : LensP a b ((a, c), d) ((b, c), d)
piP11 = first . first

maybeP : PrismP a b (Maybe a) (Maybe b)
maybeP = prismC2P maybePrism

||| Map the first element of a Maybe pair
firstMaybe : Cartesian p => CoCartesian p => Optic p a b (Maybe (a, c)) (Maybe (b, c))
firstMaybe = maybeP . first


||| Map the first element of a pair which is a maybe
maybeFirst : Cartesian p => CoCartesian p => Optic p a b ((Maybe a, c)) ((Maybe b, c))
maybeFirst = first . maybeP
