module DependentTraversal

import Dependent
import ProfunctorOptics
import Data.Vect
import Isomorphism

%hide Prelude.(*)

ExistentialTraversal : (a, b, s, t : Type) -> Type
ExistentialTraversal a b s t = s -> (n : Nat ** (Vect n a, Vect n b -> t))

TraversalDep : (a, b, s, t : Type) -> Type
TraversalDep a b s t = DepLens (n : Nat ** Vect n a) (\x => Vect (DPair.fst x) b) s (const t)


fromDepToPlain : DepParLens () (Const ())
                            (DPair Nat (\n => Vect n a)) (\x => Vect (x .fst) b)
                            s (\value => t)
                         -> s
                         -> DPair Nat (\n => (Vect n a, Vect n b -> t))
fromDepToPlain (MkDepLens get set) y with (get (() && y))
  fromDepToPlain (MkDepLens get set) y | getResult =
    ((get (() && y)).fst ** ((get (() && y)).snd, p2 . set () y))

plainToDep : (s -> DPair Nat (\n => (Vect n a, Vect n b -> t)))
          -> DepParLens () (Const ())
                        (DPair Nat (\n => Vect n a)) (\x => Vect (x .fst) b)
                        s (\value => t)
plainToDep f = MkDepLens (\arg => ((f arg.snd).fst ** fst (f arg.snd).snd))
                         (\_, arg, vec => () && (snd (f arg).snd) vec)

depLensEq : {s, a : Type} -> {b : a -> Type} -> {t : s -> Type} -> {g1, g2 : () * s -> a}
         -> {s1 : () -> (x : s) -> b (g1 (() && x)) -> () * t x}
         -> {s2 : () -> (x : s) -> b (g2 (() && x)) -> () * t x}
         -> (g1 = g2) -> (s1 = s2) -> MkDepLens {q = Const ()} {t} g1 s1 = MkDepLens g2 s2

{-

fromTo : (x : DepParLens () (Const ()) (DPair Nat (\n => Vect n a)) (\x => Vect (x.fst) b) s (\value => t))
      -> MkDepLens (\arg => MkDPair ((fromDepToPlain x (arg.snd)).fst) (fst ((fromDepToPlain x (arg.snd)).snd))) (\_, arg, vec => () && snd ((fromDepToPlain x arg).snd) vec) = x
fromTo (MkDepLens get set) = depLensEq ?whui ?whui


toFrom : (x : (s -> DPair Nat (\n => (Vect n a, Vect n b -> t))))
    -> fromDepToPlain (MkDepLens (\arg => MkDPair ((x (arg .snd)) .fst) (fst ((x (arg .snd)) .snd))) (\_, arg, vec => () && snd ((x arg) .snd) vec)) = x

traversalDependentIso : (TraversalDep a b s t) ~= (ExistentialTraversal a b s t)
traversalDependentIso = MkIso
  fromDepToPlain
  plainToDep
  fromTo
  toFrom


