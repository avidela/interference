module Optics.Lens

import Data.Boundary

public export
record Optic (l, r : Boundary) where
  constructor MkOptic
  internal : Type
  forward : l.p1 -> (r.p1, internal)
  backward : (r.p2, internal) -> l.p2

Writer : Type -> Type -> Type
Writer tape value = (value, tape)

Reader : Type -> Type -> Type
Reader env value = env -> value

composeWriter : (x -> Writer a b) -> (b -> Writer c d) -> x -> Writer (a, c) d
composeWriter f g y = let w = f y
                          v = g (fst w) in (fst v, snd w, snd v)

composeReader : (x -> Reader a b) -> (b -> Reader c d) -> x -> Reader (c, a) d
composeReader f g y z = g (f y (snd z)) (fst z)

export
compose : Optic l x -> Optic x r -> Optic l r
compose a b = MkOptic (a.internal, b.internal)
                      (composeWriter a.forward b.forward)
                      (uncurry $ composeReader (curry b.backward) (curry a.backward))

export
over : Optic l r -> (r.p1 -> r.p2) -> l.p1 -> l.p2
over optic f = optic.backward . mapFst f . optic.forward
