module Data.Boundary

public export
record Boundary where
  constructor MkB
  p1 : Type
  p2 : Type

--  x y * a b = (x, a) (y, b)
export
product : Boundary -> Boundary -> Boundary
product b1 b2 = MkB (b1.p1, b2.p1) (b1.p2, b2.p2)
