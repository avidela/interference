module Data.ListType

import Data.List
import Data.List1
import Data.Product

%default total

public export
FromList1 : List1 Type -> Type
FromList1 (head ::: []) = head
FromList1 (head ::: (x :: xs)) = head * (assert_total $ FromList1 (x ::: xs))

public export
FromList : List Type -> Type
FromList [] = ()
FromList (x :: xs) = x * FromList xs

public export
ProdList : List Type -> Type
ProdList = FromList

public export
split : {b : _} -> FromList1 (a ::: b) -> a * FromList b
split x {b = []} = x && ()
split x {b = (y :: xs)} = mapSnd split x

public export
merge : {b : _} -> a * FromList b -> FromList1 (a ::: b)
merge (fst && snd) {b = []} = fst
merge (fst && snd) {b = (x :: xs)} = fst && merge snd

public export
asPairs : FromList1 ((s * z) ::: (p ++ p'))
       -> FromList1 (s ::: p) * FromList1 (z ::: p')
asPairs x = ?asPairs_rhs

public export
toPairs : FromList1 (a ::: b) -> FromList1 (x ::: y)
       -> FromList1 (a * x ::: (b ++ y))
toPairs z w = ?toPairs_rhs

public export
swap2 : {c : _} -> a * FromList1 (b ::: c) -> b * FromList1 (a ::: c)
swap2 (fst && snd) {c = []} = snd && fst
swap2 (x && (y && z)) {c = (l :: ls)} = y && (x && z)

public export
mapHead : {a : _} -> (x -> y) -> FromList1 (x ::: a) -> FromList1 (y ::: a)
mapHead f z {a = []} = f z
mapHead f z {a = (w :: xs)} = mapFst f z

-- extract the first element of a `FromList1`
public export
listHead : {b : _} -> FromList1 (a ::: b) -> a
listHead x with (b)
  listHead x | [] = x
  listHead (fst && snd) | (y :: xs) = fst

public export
listTail : {b : _} -> FromList1 (a ::: b) -> FromList b
listTail x {b = []} = ()
listTail x {b = (y :: xs)} = split (p2 x)

public export
mapTail : {a, b : _}
       -> (FromList a -> FromList b)
       -> FromList1 (x ::: a) -> FromList1 (x ::: b)
mapTail f y {a = []} = ListType.merge (y && f ())
mapTail f y {a = (z :: xs)} = mapTail (f . (listHead (p2 y) &&))
                                      (p2 $ swap2 y)

namespace FromList1
  public export
  insert : {bs, xs : _} -> z * FromList1 (b ::: (bs ++ xs)) -> FromList1 (b ::: (bs ++ (z :: xs)))
  insert (fst && snd) {bs = []} {xs = []} = snd && fst
  insert (fst && (y && snd)) {bs = []} {xs = (x :: xs)} = y && (fst && snd)
  insert (fst && (y && snd)) {bs = (x :: ys)} = y && insert (fst && snd)

  public export
  flipTail : {a, b : _} -> FromList1 (x ::: (a ++ b)) -> FromList1 (x ::: (b ++ a))
  flipTail y {a = []} = rewrite appendNilRightNeutral b in y
  flipTail (fst && snd) {a = (z :: xs)} {b =[]}   = let rec = flipTail snd in fst && rec
  flipTail (fst && snd) {a = (z :: xs)} {b=b::bs} = let rec = flipTail snd in fst && insert rec

  public export
  concat : {a, b : _} -> FromList1 (x ::: a) -> FromList1 (y ::: b) -> FromList1 (x ::: (a ++ b))
  concat z w {a = []} {b = []} = z
  concat z (f && s) {a = []} {b = (v :: xs)} = z && s
  concat (fst && snd) w {a = (v :: xs)} {b = b} = fst && FromList1.concat snd w

namespace FromList

  public export
  split : {a : _} -> FromList (a ++ b) -> FromList a * FromList b
  split x {a = []} = () && x
  split x {a = (y :: xs)} = let s = FromList.split (p2 x) in
                                mapFst (p1 x &&) s

  public export
  insert : {b : _} -> y -> FromList (b ++ xs) -> FromList (b ++ (y :: xs))
  insert x z {b = []} = x && z
  insert x z {b = (w :: ys)} = mapSnd (insert x) z

  public export
  flipLists : {a, b : _} -> FromList (a ++ b) -> FromList (b ++ a)
  flipLists x {a = []} = rewrite appendNilRightNeutral b in x
  flipLists x {a = (y :: xs)} = let res = flipLists (p2 x)
                                 in insert (p1 x) res

  public export
  concat : {a, b : _} -> FromList a -> FromList b -> FromList (a ++ b)
  concat x y {a = []} = y
  concat x y {a = (z :: xs)} = mapSnd (flipLists . FromList.concat y) x

public export
Pointwise : {a, b : _} -> (FromList a -> Type)
                       -> (FromList b -> Type)
                       -> FromList (a ++ b) -> Type
Pointwise f g x = f (p1 $ FromList.split x) * g (p2 $ FromList.split x)
