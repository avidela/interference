module Data.Product

%hide Prelude.(&&)

infixl 10 ^
infixl 8 &&
infixr 9 +/

||| A product type for path components
public export
record (+/) (a, b : Type) where
  constructor MkPathExt
  head : a
  tail : b

public export
Show a => Show b => Show (a +/ b) where
  show (MkPathExt a b) = "\{show a} / \{show b}"

||| A product of two types
public export
record (*) (a, b : Type) where
  constructor (&&)
  fst : a
  snd : b

infix 8 `×`

public export
× : (a : Type) -> (b : a -> Type) -> Type
× = DPair

public export
merge : (a -> c) -> (b -> c) -> (c -> c -> c) -> a * b -> c
merge f g h (fst && snd) = f fst `h` g snd

public export
Show a => Show b => Show (a * b) where
  show (a && b) = "\{show a} & \{show b}"

public export
p1 : a * b -> a
p1 = .fst

public export
p2 : a * b -> b
p2 = .snd

public export
swap : a * b -> b * a
swap (fst && snd) = snd && fst

public export
toPair : a * b -> (a, b)
toPair (a && b) = (a, b)

public export
fromPair : (a, b) -> (a * b)
fromPair (a, b) = a && b

public export
Bifunctor (*) where
  bimap f g = fromPair . bimap f g . toPair

public export
biFirst : Bifunctor bi => (a -> b) -> bi a c -> bi b c
biFirst f = bimap f id

public export
biSecond : Bifunctor bi => (a -> b) -> bi c a -> bi c b
biSecond f = bimap id f

public export
dup : a -> a * a
dup x = x && x

public export
distribute : a * b -> c * d -> (a * c) * (b * d)
distribute (x && y) (z && w) = (x && z) && (y && w)

export
fork : (a -> b) -> (a -> c) -> a -> b * c
fork f g x = f x && g x

export
split : (a * b) * c -> (a * c) * (b * c)
split ((a && b) && c) = (a && c) && (b && c)

||| Like bimap but with two arguments
export
through : (a -> b -> c) -> (x -> y -> z) -> (a * x) -> (b * y) -> (c * z)
through f g (a && x) (b && y) = f a b && g x y

||| from arity 2 to arity 1 with pair
public export
curry : (a * b -> c) -> a -> b -> c
curry f a b = f (a && b)

||| from arity 2 to arity 1 with pair
public export
uncurry : (a -> b -> c) -> a * b -> c
uncurry f (fst && snd) = f fst snd

public export
elim : (a -> a') -> (b -> b') -> (a' -> b' -> c) -> a * b -> c
elim f g m (p1 && p2) = m (f p1) (g p2)

public export
bi : (a -> a') -> (b -> b') -> a * b -> a' * b'
bi f1 f2 p = elim f1 f2 (&&) p

public export
shuffle : (a * x) * (b * y) -> (a * b) * (x * y)
shuffle ((a && x) && (b && y)) = (a && b) && (x && y)

public export
proj1Pair : (0 a, b : _) -> Product.p1 (a && b) === a
proj1Pair _ _ = Refl

public export
proj2Pair : (0 a, b : _) -> Product.p2 (a && b) === b
proj2Pair _ _ = Refl

public export
(^) : Type -> Nat -> Type
(^) a Z = Unit
(^) a (S 1) = a
(^) a (S n) = a * a ^ n


public export
FreeProd : List Type -> Type
FreeProd [] = Unit
FreeProd (x :: []) = x
FreeProd (x :: (y :: xs)) = x * FreeProd (y :: xs)

