module Data.Sum

import Data.List

prefix 6 <+, +>

public export
data (+) : Type -> Type -> Type where
  (<+) : a -> a + b
  (+>) : b -> a + b

public export
choice : (a -> c) -> (b -> c) -> a + b -> c
choice f g (<+ x) = f x
choice f g (+> x) = g x

public export
elim : (a -> c) -> (b -> d) -> a + b -> c + d
elim f g (<+ x) = <+ (f x)
elim f g (+> x) = +> (g x)

public export
Bifunctor (+) where
  bimap = elim

public export
SumList : List Type -> Type
SumList [] = Void
SumList (x :: xs) = x + SumList xs

public export
match : {a : _} -> SumList a -> SumList b -> SumList (a ++ b)
match {a = []} x y = y
match {a = (ty :: xs)} (<+ x) y = <+ x
match {a = (ty :: xs)} (+> x) y = +> match x y

split : {a : _} -> SumList (a ++ b) -> SumList a + SumList b
split {a = []} x = +> x
split {a = (y :: xs)} (<+ x) = <+ <+ x
split {a = (y :: xs)} (+> x) = case split x of
                                    (<+ z) => <+ (+> z)
                                    (+> z) => +> z


public export
FreeSum : List Type -> Type
FreeSum [] = Void
FreeSum [x] = x
FreeSum (x :: y :: xs) = x + FreeSum (y :: xs)

public export
splitSum : {a, b : List Type} -> FreeSum (a ++ b) -> FreeSum a + FreeSum b
splitSum x {a = []} = +> x
splitSum x {a = (y :: xs)} {b = []} = <+ (rewrite sym $ appendNilRightNeutral xs in x)
splitSum (<+ x) {a = (y :: [])} {b = (z :: ys)} = <+ x
splitSum (+> x) {a = (y :: [])} {b = (z :: ys)} = +> x
splitSum (<+ x) {a = (y :: (w :: xs))} {b = (z :: ys)} = <+ <+ x
splitSum (+> x) {a = (y :: (w :: xs))} {b = (z :: ys)} =
  case splitSum {a=w::xs} {b = z :: ys} x of
       (+> x') => +> x'
       (<+ x') => <+ (+> x')

public export
sum : FreeSum a + FreeSum b -> FreeSum (a ++ b)

public export
ChooseSum : {a, a' : List Type} -> (FreeSum a -> Type) -> (FreeSum a' -> Type) -> FreeSum (a ++ a') -> Type
ChooseSum f g x {a = []} {a' = []} = absurd x
ChooseSum f g x {a = []} {a' = (y :: xs)} = g x
ChooseSum f g x {a = (y :: xs)} {a' = []} = f (rewrite sym $ appendNilRightNeutral xs in x)
ChooseSum f g x {a = (y :: xs)} {a' = (z :: ys)} = Sum.choice f g (splitSum x)

export
Show a => Show b => Show (a + b) where
  show (<+ l) = show l
  show (+> r) = show r
