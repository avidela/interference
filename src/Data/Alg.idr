module Data.Alg

import Data.Sum
import Data.Product

public export
distributePlus : a * (b + c) -> a * b + a * c
distributePlus (fst && (+> x)) = +> (fst && x)
distributePlus (fst && (<+ x)) = <+ (fst && x)

||| distributive property of products over coproducts
public export
distributive : (p * p') * (s + s') -> (p * s) + (p' * s')
distributive ((fst && snd) && (<+ x)) = <+ (fst && x)
distributive ((fst && snd) && (+> x)) = +> (snd && x)

