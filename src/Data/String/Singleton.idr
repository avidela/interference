module Data.String.Singleton

import Interfaces
import Data.String.Parser
import Data.String.ParserInterface
import Debug.Trace

public export
data Str : String -> Type where
  MkS : (str : String) -> Str str

export
{s : String} -> Default (Str s) where
  def = MkS s

export
Show (Str s) where
  show (MkS s) = s

export
{s : String} -> Display (Str s) where
  display = "Str \{s}"

export
{s : String} -> HasParser (Str s) where
  partialParse = string s *> pure (MkS s)
