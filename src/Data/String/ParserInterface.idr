module Data.String.ParserInterface

import Data.SortedMap
import Data.String
import Data.String.Parser
import Data.String.ParserUtils
import Data.Product
import Data.Sum
import Data.Carrier

------------------------------------------------------------------------
-- Parser Interface
------------------------------------------------------------------------

removeCommonPrefix : (pre, input : String) -> Maybe String
removeCommonPrefix pre input =
  let pre' = unpack pre
      inp = unpack input in
      checkPrefix pre' inp
      where checkPrefix : List Char -> List Char -> Maybe String
            checkPrefix [] rest = Just $ pack rest --all good
            checkPrefix (x :: xs) [] = Nothing -- prefix longer
            checkPrefix (x :: xs) (y :: ys) = if x == y -- keep going if same
                                                 then checkPrefix xs ys
                                                 else Nothing

public export
interface HasParser t where
  constructor MkParser
  partialParse : Parser t

public export
interface IHasParser idx (0 t : idx -> Type) where
  constructor MkIParser
  iPartialParse : {v : idx} -> Parser (t v)

export
HasParser t => IHasParser s (const t) where
  iPartialParse = partialParse

export
[iparserDepSum] IHasParser a b => IHasParser a' b' => IHasParser (a + a') (Sum.choice b b') where
  iPartialParse = case v of
                       (<+ x) => iPartialParse {t = b}
                       (+> x) => iPartialParse {t = b'}
export
[iparserDepProd] IHasParser a b => IHasParser a' b' => IHasParser (a * a') (\x => Product.(*) (b (p1 x)) (b' (p2 x))) where
  iPartialParse = case v of
                       (x && y) => [| iPartialParse {t = b} && iPartialParse {t = b'} |]

export
iparse : (state : st) -> {t : st -> Type} -> (IHasParser st t) =>  String -> Either String (t state)
iparse state input = fst <$> Parser.parse (iPartialParse {v=state} <* eos) input

export
HasParser Bool where
  partialParse = exact "true" *> pure True
             <|> exact "false" *> pure False

export
HasParser Nat where
  partialParse = natural

export
HasParser Int where
  partialParse = cast <$> integer

export
HasParser Double where
  partialParse =  do
      let minus : Double = if !(succeeds (char '-')) then (-1) else 1
      leading <- option ['0'] (some (satisfy isDigit))
      ignore $ char '.'
      mantissa <- some (satisfy isDigit)
      let double = "\{pack leading}.\{pack mantissa}"
      case parseDouble double of
           Nothing => fail "expected Double, got \{double} instead"
           Just d => pure (minus * d)


export
HasParser String where
  -- we assume strings are just the whole (nonempty) input
  partialParse = takeWhile1 (\x => not (isSpace x) && x /= '/') <|> (takeWhile1 (not . isSpace) <* eos)

export
HasParser () where
  partialParse = pure ()

export
HasParser a => HasParser b => HasParser (a + b) where
  partialParse =
    ((<+) <$> partialParse {t=a})
    <|>
    ((+>) <$> partialParse {t=b})

export
implementation HasParser a => HasParser b => HasParser (a +/ b) where
    partialParse = do ignore $ char '/'
                      v1 <- partialParse {t=a}
                      v2 <- partialParse {t=b}
                      pure (MkPathExt v1 v2)

export
implementation HasParser a => HasParser b => HasParser (a * b) where
  partialParse = do
    ignore $ char '('
    v1 <- partialParse {t=a}
    ignore $ char ','
    v2 <- partialParse {t=b}
    ignore $ char ')'
    pure (v1 && v2)
export
implementation HasParser a => HasParser b => HasParser (a, b) where
  partialParse = do
    ignore $ char '('
    v1 <- partialParse {t=a}
    ignore $ char ','
    v2 <- partialParse {t=b}
    ignore $ char ')'
    pure (v1, v2)

export
implementation HasParser a => HasParser (List a) where
  partialParse = do
    ignore $ char '['
    hchainr (partialParse {t=a}) (char ',' *> pure (::)) (char ']' *> pure Nil)

-- sorted maps are parsed from lists of pairs
export
implementation Ord key => HasParser key => HasParser val => HasParser (SortedMap key val) where
  partialParse = fromList <$> partialParse

export
HasParser t => HasParser (Carrier t) where
  partialParse = Carry <$> partialParse {t}
