module Set

import Category

||| Functions
data (&->) : Type -> Type -> Type where
  Func : forall a, b.  (a -> b) -> a &-> b

||| Function Application
(|>) : forall a, b. a &-> b -> a -> b
(|>) (Func f) v = f v

||| Functional extentionality postulate
0 FunExt : forall a, b. {0 f, g : a &-> b} ->  ((v : a) -> f |> v = g |> v) -> f = g

||| Identity
fid : forall a. a &-> a
fid = Func id

||| Composition
fcomp : forall a, b, c. a &-> b -> b &-> c -> a &-> c
fcomp (Func f) (Func g) = Func (g . f)

||| Composition
(>>>) : forall a, b, c. a &-> b -> b &-> c -> a &-> c
(>>>) (Func f) (Func g) = Func (g . f)

lemma : {0 a, b : Type} -> (g : a -> b) -> Func (\x => g x) = Func g
lemma g = Refl

lidFunc : {0 a, b : Type} -> (v : a &-> b) ->  (fid {a=a})  >>> v = v
lidFunc (Func f) = Refl

0 ridFunc : {0 a, b : Type} -> (v : a &-> b) ->  v >>> (fid {a=b})  = v
ridFunc (Func f) = FunExt $ \v => Refl

||| The category of Types and functions is called "Set"
Set : Category Type
Set = MkCategory (&->)
                 (\_ => Func (\x => x))
                 fcomp
                 (\(Func f) => Refl)
                 (\(Func g) => lemma g)
                 (\(Func f'), (Func g'), (Func h') => Refl)

||| Coproducts
data (+) : Type -> Type -> Type where
  Left : forall a, b. a -> a + b
  Right : forall a, b. b -> a + b

Profunctor : {0 o : Type} -> Category o -> Type
Profunctor c = (opposite c >< c)  ~:> Set

Fn : Type -> Type -> Type
Fn a b = a -> b

proMorphism : (A : (Type, Type)) -> (B : (Type, Type))
     -> (fst B &-> fst A, snd A &-> snd B)
     -> uncurry (&->) A &-> uncurry (&->) B
proMorphism (a, b) (a', b') (f1, f2) =
    Func $ \ab => f1 >>> ab >>> f2

0 compPro : (a, b, c: (Type, Type))
    -> (F : (fst b &-> fst a, snd a &-> snd b))
    -> (G : (fst c &-> fst b, snd b &-> snd c))
    -> proMorphism a c (fcomp (fst G) (fst F), fcomp (snd F) (snd G))
     = fcomp (proMorphism a b F) (proMorphism b c G)
compPro (ax, ay) (bx, by) (cx, cy)
    ((Func f1), (Func f2)) ((Func g1), (Func g2)) =
      FunExt $ \(Func k) => Refl

SetPro :  Profunctor Set
SetPro = MkFun
    (uncurry (&->))
    proMorphism
    (\(a, b) => (FunExt $ \v => let k = ridFunc v in
                                let l = lidFunc v in
                                rewrite k in
                                rewrite l in Refl))
    compPro

ProfunctorType : (o : Type) -> (rel, p: o -> o -> Type) -> Type
ProfunctorType _ rel p =
  forall a, a', b, b'. (rel a' a) -> (rel b b') -> p a b -> p a' b'

record ProfunctorData (rel, p : Type -> Type -> Type) where
  constructor MkPro
  dimap : ProfunctorType Type rel p


  {-

--   dimapId : {a, b : Type} ->
--             dimap {a=a} {a'=a} {b=b} {b'=b} Prelude.id Prelude.id = Prelude.id
--   dimapComp : {0 a, a', ai, b, b', bi : Type}
--            -> (f' : ai -> a) -> (f : a' -> ai)
--            -> (g' : b -> bi) -> (g : bi -> b')
--            -> dimap (f' . f) (g . g') = dimap f g . dimap f' g'

-- set is profunctor
%hint
SetIsPro' : ProfunctorData Fn Fn
SetIsPro' = MkPro (\f, g, f1 => \x => g (f1 (f x)))

SetIsPro : ProfunctorData (&->) (&->)
SetIsPro = MkPro (\f, g, h => f `fcomp` (h `fcomp` g))

fromCat : Profunctor o cat -> ProfunctorData o (morphism cat) (morphism cat)
fromCat (MkFun toObj toMor keepIdentity keepComposition) =
  MkPro $ \x, y, z => ?fromCat_rhs_1

record Cartesian (rel, p : Type -> Type -> Type)
                 (pro : ProfunctorData rel p)  where
  constructor MkCart
  first : forall a, b, c. rel (p a b) (p (a, c) (b, c))
  second : forall a, b, c. rel (p a b) (p (c, a) (c, b))

record CoCartesian (rel, p : Type -> Type -> Type)
                   (pro : ProfunctorData rel p)  where
  constructor MkCoCart
  left  : forall a, b, c. rel (p a b) (p (a + c) (b + c))
  right : forall a, b, c. rel (p a b) (p (c + a) (c + a))
  {-
  -}
