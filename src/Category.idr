
module Category

%default total
-- %unbound_implicits off
%hide Either
%hide Left
%hide Right

-- operator for composition
infix 6 .*.
infix 6 -*-
-- operator for morphisms
infixr 5 ~>
infixr 5 ~!>
infixr 5 ~:>
infixr 5 ==>
infixr 5 #->
infixr 5 &->
infixr 4 |:
infixr 4 |>
infixr 7 ><
infixr 7 >>>


namespace Cat
  public export
  record Category (obj : Type)  where
    constructor MkCategory
    (~>) : obj -> obj -> Type
    identity      : (0 a : obj) -> a ~> a
    (.*.)         : {0 a, b, c: obj}
                 -> (a ~> b)
                 -> (b ~> c)
                 -> (a ~> c)
    0 leftIdentity  : {a, b : obj}
                 -> (f : a ~> b)
                 -> identity a .*. f = f
    0 rightIdentity : {a, b : obj}
                 -> (f : a ~> b)
                 -> f .*. identity b = f
    0 associativity : {0 a, b, c, d : obj}
                 -> (f : a ~> b)
                 -> (g : b ~> c)
                 -> (h : c ~> d)
                 -> f .*. (g .*. h) = (f .*. g) .*. h

public export
morphism : Category o -> o -> o -> Type
morphism = (~>)

public export
(|:) : {0 o, returnType : Type} -> Category o -> returnType -> returnType
(|:) cat t = t

public export
(~!>) : {0 obj : Type} -> (cat : Category obj) => obj -> obj -> Type
(~!>) = (~>) cat

public export
(-*-) : {0 obj : Type} -> (cat : Category obj) => {0 a, b, c : obj}
     -> (~>) cat a b -> (~>) cat b c -> (~>) cat a c
(-*-) = Category.(.*.) cat {a} {b} {c}

||| Functors
public export
record (~:>) {0 Obj1 : Type} {0 Obj2 : Type} (C1 : Category Obj1) (C2 : Category Obj2) where
   constructor MkFun
   toObj : Obj1 -> Obj2
   toMor : (A, B : Obj1) -> C1 |: A ~!> B -> C2 |: (toObj A) ~!> (toObj B)
   0 keepIdentity : (A : Obj1) -> toMor A A (identity C1 A ) = (identity C2 (toObj A) )
   0 keepComposition : (a, b, c : Obj1) -> (F : (~>) C1 a b) -> (G : (~>) C1 b c) ->
                       toMor a c ((.*.) C1 {a} {b} {c} F G)
                        =
                       (.*.) C2 {a=toObj a} {b=toObj b} {c=toObj c} (toMor a b F) (toMor b c G)


public export
opposite : forall o. Category o -> Category o
opposite (MkCategory mor idt comp lid rid assoc) =
  MkCategory (\a, b => mor b a) idt (\f, g => comp g f)
             (\f => rid f) (\f => lid f) (\f', g', h' => sym $ assoc h' g' f' )

||| Product Category
public export
(><) : forall a, b. Category a -> Category b -> Category (a, b)
(><) (MkCategory mor1 id1 comp1 lid1 rid1 ass1)
        (MkCategory mor2 id2 comp2 lid2 rid2 ass2) =
          MkCategory (\p1, p2 => Pair (mor1 (fst p1) (fst p2)) (mor2 (snd p1) (snd p2)))
                     (\a => (id1 (fst a), id2 (snd a)))
                     (\f, g => (comp1 (fst f) (fst g) , comp2 (snd f) (snd g)))
                     (\(l, r) => let left = lid1 l
                                     right = lid2 r in cong2 (,) left right)
                     (\(l, r) => let left = rid1 l
                                     right = rid2 r in cong2 (,) left right)
                     (\f, g, h => cong2 (,) (ass1 (fst f) (fst g) (fst h)) (ass2 (snd f) (snd g) (snd h)))

