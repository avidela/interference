||| DepParLens :     Dependent parameters &     dependent arguments
||| -          :     Dependent parameters & non-dependent arguments
||| DepPara    : Non-dependent parameters &     dependent arguments
|||    Para    : Non-dependent parameters & non-dependent arguments
||| DepLens    :            No parameters &     dependent arguments
|||    Lens    :            No parameters & non-dependent arguments
module Dependent

import public Data.Product
import public Data.Sum
import public Data.Alg

%default total

||| Dependent lenses with dependent parameters
public export
record DepParLens (p : Type) (q : p -> Type)
                  (a : Type) (b : a -> Type)
                  (s : Type) (t : s -> Type)  where
  constructor MkDepLens
  get : p * s -> a
  set : (pv : p) -> (es : s) -> b (get (pv && es)) -> q pv * t es

  -- todo: try this signature
  -- set : (x : p * s) -> b (get x) -> q (p1 x) * t (p2 x)

public export
Const : a -> b -> a
Const v _ = v

||| update the parameter across a lens
export
reparam : DepParLens p q a b s t
       -> DepParLens Unit (Const Unit) p q p' q'
       -> DepParLens p' q' a b s t
reparam (MkDepLens get1 set1) (MkDepLens get2 set2) =
  MkDepLens (get1 . mapFst (get2 . (() &&)))
            (\pv, es, b => let qt = set1 (get2 (() && pv)) es b
                               res = set2 () pv qt.fst
                            in res.snd && qt.snd)

setRes : forall a, p, p', x, s
      . {0 b : a -> Type}
      -> {0 q : p -> Type}
      -> {0 t : s -> Type}
      -> {0 q' : p' -> Type}
      -> {0 y : x -> Type}
      -> (get1 : p * s -> a)
      -> (set1 : (pv : p) -> (es : s) -> b (get1 (pv && es)) -> q pv * t es)
      -> (get2 : p' * x -> s)
      -> (set2 : (pv : p') -> (es : x) -> t (get2 (pv && es)) -> q' pv * y es)
      -> (pv : p * p')
      -> (es : x)
      -> b (get1 (p1 pv && get2 (p2 pv && es)))
      -> (q (p1 pv) * q' (p2 pv)) * y es
setRes get1 set1 get2 set2 pv es z =
  let (res && cont) = set1 (p1 pv) (get2 (p2 pv && es)) z
      (r1 && r2) = set2 (p2 pv) es (cont) in (res && r1) && r2

||| Sequential composition of dependent lenses with parameters
||| The parameters get tensored to represent the fact that
||| we need to handle both of them at once, one for each lens
--
--            p × p'                   q × q'
--             │ │                      │ │
--       ┌───────────────────────────────────────┐
--       │     │ │                      │ │      │
--       │     │ ╰──────────────────╮   │ │      │
--       │     │     ┌──────────────────╯ │      │
--       │     │     ▲              │     ▲      │
--       │  ┌──┴─────┴───┐       ┌──┴─────┴───┐  │
--   a ──┤──┤            ├─> s ──┤            ├──├─> x
--       │  │            │       │            │  │
--   b <─┤──┤            ├── t <─┤            ├──├── y
--       │  └────────────┘       └────────────┘  │
--       └───────────────────────────────────────┘
export
compose : {0 b : a -> Type}
    -> {0 q : p -> Type}
    -> {0 t : s -> Type}
    -> {0 q' : p' -> Type}
    -> {0 y : x -> Type}
    -> DepParLens p q a b s t
    -> DepParLens p' q' s t x y
    -> DepParLens (p * p') (\pv => q (p1 pv) * q' (p2 pv))
                  a b
                  x y
compose (MkDepLens get1 set1) (MkDepLens get2 set2) =
  MkDepLens (\pv => get1 ((p1 (p1 pv)) && get2 ((p2 (p1 pv)) && (p2 pv))))
            (let v = Dependent.setRes {b} get1 set1 get2 set2 in
                 \pv, x, bee => v pv x (rewrite sym $ proj1Pair pv x in
                                       replace {p = \k => b (get1 (p1 (p1 (pv && x)) && get2 (p2 (p1 (pv && x)) && k)))}
                                               (proj2Pair pv x) bee))
||| Composition of lenses with a constant parameters
export
composeClone :
  DepParLens st (Const q) a b s t ->
  DepParLens st (Const q) s t x y ->
  DepParLens st (Const q) a b x y
composeClone (MkDepLens get1 set1) (MkDepLens get2 set2) =
  MkDepLens (\arg => get1 (p1 arg && get2 arg))
            (\state, val, bval => let s1 = set1 state (get2 (state && val)) bval in set2 state val (p2 s1))

||| Dependent lenses without parameters
namespace DepLens

    ||| Dependent lenses alias
    public export
    DepLens : (a : Type) -> (b : a -> Type) -> (s : Type) -> (t : s -> Type) -> Type
    DepLens = DepParLens Unit (Const Unit)

    ||| Composition of dependent lenses
    public export
    compose : DepLens a b s t -> DepLens s t x y -> DepLens a b x y
    compose l1 l2 = reparam (Dependent.compose l1 l2) (MkDepLens id (const (const id)))

||| Dependent lenses with non-dependent parameters
namespace DepPara

    public export
    DepPara : (p, q : Type) -> (a : Type) -> (b : a -> Type) -> (s : Type) -> (t : s -> Type) -> Type
    DepPara p q a b s t = DepParLens p (Const q) a b s t

    public export
    compose : DepPara p q a b s t -> DepPara p' q' s t x y -> DepPara (p * p') (q * q') a b x y
    compose l1 l2 = Dependent.compose l1 l2

    public export
    composeClone : DepPara p q a b s t -> DepPara p q s t x y -> DepPara p q a b x y
    composeClone l1 l2 = Dependent.composeClone l1 l2

||| Non-dependent lenses with non-dependent parameters
namespace Para

    ||| Para lenses alias
    -- get : p * s -> a
    -- set : (pv : p) -> (es : s) -> b -> p * t
    public export
    Para : (p, q, a, b, s, t : Type) -> Type
    Para p q a b s t = DepParLens p (Const q) a (Const b) s (Const t)

    public export
    compose : Para p q a b s t -> Para p' q' s t x y -> Para (p * p') (q * q') a b x y
    compose l1 l2 = Dependent.compose l1 l2

    -- obtain and update the parameter, discard the incoming value, and
    -- use the new value for update s (the return of get) and t are left untouched
    public export
    parameter : Para st st st st x x
    parameter = MkDepLens p1 (const (flip (&&)))

    public export
    parameterUnit : Para st st st st () st
    parameterUnit = MkDepLens p1 (const (const dup))

||| Non-dependent lenses without parameters
namespace Plain
    public export
    Lens : (a, b, s, t : Type) -> Type
    Lens a b s t = DepLens a (Const b) s (Const t)

    public export
    MkLens : (s -> a) -> (s -> b -> t) -> Lens a b s t
    MkLens f g = MkDepLens (f . p2) (const ((() &&) .: g))

    public export
    compose : Lens a b s t -> Lens s t x y -> Lens a b x y
    compose l1 l2 = DepLens.compose l1 l2

--
--          p     q                      s     t
--          ╷     ▲                      ╷     ▲
--       ┌──┴─────┴───┐               ┌──┴─────┴───┐
--   s ──┤            ├─> a       p ──┤            ├─> a
--       │            │      ==>      │            │
--   t <─┤            ├── b       q <─┤            ├── b
--       └────────────┘               └────────────┘
--
public export
swapParameters : DepParLens p q a b s t -> DepParLens s t a b p q
swapParameters (MkDepLens s g) = MkDepLens
    (s . swap)
    (\st, es => swap . g es st)

public export
extRes : forall p
    . {0 q : p -> Type}
   -> {0 t : s -> Type}
   -> {0 y : x -> Type}
   -> (get1 : p * s -> a)
   -> (set1 : (pv : p) -> (es : s) -> b (get1 (pv && es)) -> q pv * t es)
   -> (get2 : p * x -> c)
   -> (set2 : (pv : p) -> (es : x) -> d (get2 (pv && es)) -> q pv * y es)
   -> (pv : p) -> (es : s + x) -> Sum.choice b d (Sum.elim get1 get2 (Alg.distributePlus (pv && es))) -> (q pv) * Sum.choice t y es
extRes get1 set1 get2 set2 pv (+> w) z = set2 pv w z
extRes get1 set1 get2 set2 pv (<+ w) z = set1 pv w z

||| External choice of two lenses with a common parameter.
export
extChoiceClone : forall a, p, x
     . {0 b : a -> Type}
    -> {0 q : p -> Type}
    -> {0 t : s -> Type}
    -> {0 y : x -> Type}
    -> DepParLens p q a b s t
    -> DepParLens p q c d x y
    -> DepParLens p (\st => q st)
                  (a + c) (Sum.choice b d)
                  (s + x) (Sum.choice t y)
extChoiceClone (MkDepLens get1 set1) (MkDepLens get2 set2) =
  MkDepLens (Sum.elim get1 get2 . Alg.distributePlus)
            (extRes get1 set1 get2 set2)

extMore :
      {0 q : p -> Type}
   -> {0 q' : p' -> Type}
   -> (get1 : p * s -> a)
   -> (set1 : (pv : p) -> (es : s) -> b (get1 (pv && es)) -> q pv * t es)
   -> (get2 : p' * s' -> a')
   -> (set2 : (pv : p') -> (es : s') -> b' (get2 (pv && es)) -> q' pv * t' es)
   -> (pv : p * p') -> (es : s + s')
   -> Sum.choice b b' (Sum.elim get1 get2 (Alg.distributive (pv && es)))
   -> (q (Product.p1 pv) + q' (Product.p2 pv)) * Sum.choice t t' es
extMore get1 set1 get2 set2 (a && b) (<+ x) ch = mapFst (<+) (set1 a x ch)
extMore get1 set1 get2 set2 (a && b) (+> x) ch = mapFst (+>) (set2 b x ch)

||| External choice of two lenses, picks one of the two lenses and runs it
||| The left boundaries are coproduct to indicate that we need to pick
||| from either the `ks` or `ls` lens. The parameters `p` are tensored to
||| indicate that we must be ready to handle either inputs. The parameters
||| `q` are co-product to indicate that once the update has been performed
||| only one the corresponding parameter from the lens `ks` or `ls` needs to
||| be updated
--
--                  p × p'                 q + q'
--                   │ │                    │ │
--       ┌───────────────────────────────────────────────────┐
--       │           │ ╰────────────────╮   │ │              │
--       │           │     ╭────────────│───╯ │              │
--       │           │     ▲            │     │              │
--       │        ┌──┴─────┴───┐        │     │              │
--       │  ╭─╴s╶─┤            ├─> a ───│─────│──────────╮   │
--       │  │     │     ks     │        │     │          │   │
--       │  │╭╴t<─┤            ├── b ───│─────│────────────╮ │
-- s+s' ═╡══╡│    └────────────┘        │     │          ╞═│═╞═> a+a'
--       │  ││                          │     ▲          │ │ │
-- t+t'<═╡══│╡                       ┌──┴─────┴───┐      │ ╞═╞══ b+b'
--       │  ╰────────────────── s'╶──┤            ├─> a'╶╯ │ │
--       │   │                       │     ls     │        │ │
--       │   ╰───────────────── t' <─┤            ├──╴b'╶──╯ │
--       │                           └────────────┘          │
--       └───────────────────────────────────────────────────┘
export
extChoice : forall a, p, p'
     . {0 b : a -> Type}
    -> {0 q : p -> Type}
    -> {0 q' : p' -> Type}
    -> DepParLens p  q  a  b  s  t
    -> DepParLens p' q' a' b' s' t'
    -> DepParLens (p * p') (\st => q (p1 st) + q' (p2 st))
                  (a + a') (Sum.choice b b')
                  (s + s') (Sum.choice t t')
extChoice (MkDepLens get1 set1) (MkDepLens get2 set2) =
  MkDepLens (Sum.elim get1 get2 . Alg.distributive)
            (extMore get1 set1 get2 set2)

tensorSet : forall p, p', q', q.
               {0 a : Type} -> {0 b : a -> Type}
            -> {0 s : Type} -> {0 t : s -> Type}
            -> {0 x : Type} -> {0 y : x -> Type}
            -> {0 z : Type} -> {0 w : z -> Type}
            -> (get1 : p * s -> a)
            -> (set1 : (pv : p) -> (es : s) -> b (get1 (pv && es)) -> q pv * t es)
            -> (get2 : p' * z -> x)
            -> (set2 : (pv : p') -> (es : z) -> y (get2 (pv && es)) -> q' pv * w es)
            -> (pv : p * p')
            -> (es : s * z)
            -> b ((elim get1 get2 (&&) (shuffle (pv && es))).fst) * y ((elim get1 get2 (&&) (shuffle (pv && es))).snd)
            -> (q (pv.fst) * q' (pv.snd)) * (t (es.fst) * w (es.snd))
tensorSet get1 set1 get2 set2 (pv1 && pv2) (es1 && es2) (v1 && v2) = shuffle (set1 pv1 es1 v1 &&
                                                                              set2 pv2 es2 v2)

||| Tensor of two lenses, runs both of them at the same time
--
--                  p × p'                    q × q'
--                   │ │                       │ │
--       ┌──────────────────────────────────────────────────────┐
--       │           │ ╰───────────────────╮   │ │              │
--       │           │     ╭───────────────────╯ │              │
--       │           │     ▲               │     │              │
--       │        ┌──┴─────┴───┐           │     │              │
--       │  ╭─╴s╶─┤            ├─> a ───────────────────────╮   │
--       │  │     │            │           │     │          │   │
--       │  │╭╴t<─┤            ├── b ──────│─────│────────────╮ │
-- s×s' ═╡══╡│    └────────────┘           │     │          ╞═│═╞═> a×a'
--       │  ││                             │     │          │ │ │
-- t×t'<═╡══│╡                             │     ▲          │ ╞═╞══ b×b'
--       │  ││                          ┌──┴─────┴───┐      │ │ │
--       │  ╰───────────────────── s'╶──┤            ├─> a'╶╯ │ │
--       │   │                          │            │        │ │
--       │   ╰──────────────────── t' <─┤            ├──╴b'╶──╯ │
--       │                              └────────────┘          │
--       └──────────────────────────────────────────────────────┘
export
tensor : DepParLens p q a b s t
      -> DepParLens p' q' x y z w
      -> DepParLens (p * p') (\x => q x.fst * q' x.snd)
                    (a * x) (\v  => b v.fst * y v.snd)
                    (s * z) (\v => t v.fst * w v.snd)
tensor (MkDepLens get1 set1) (MkDepLens get2 set2) =
  MkDepLens (elim get1 get2 (&&) . shuffle)
            (tensorSet {q} {p} {p'} {a} {b} {s} {t} {x} {y} get1 set1 get2 set2)

export
removeUnitRight : DepParLens () (Const ()) (p * ()) (\x => q (x .fst) * ()) p q
removeUnitRight = MkDepLens swap (const (\x => swap))

export
removeUnitLeft : DepParLens () (Const ()) (() * p) (\x => () * q (x .snd)) p q
removeUnitLeft = MkDepLens id (const (\x => id))

||| Tensor a lens with parameters with a lens without parameters on the right
export
tensorLensRight : DepParLens p q a b s t
      -> DepParLens () (const ()) x y z w
      -> DepParLens p q
                    (a * x) (\v  => b v.fst * y v.snd)
                    (s * z) (\v => t v.fst * w v.snd)
tensorLensRight l1 l2 = reparam (tensor l1 l2) removeUnitRight

||| Tensor a lens with parameters with a lens without parameters on the left
export
tensorLensLeft : DepParLens () (const ()) x y z w
      -> DepParLens p q a b s t
      -> DepParLens p q
                    (x * a) (\v  => y v.fst * b v.snd)
                    (z * s) (\v => w v.fst * t v.snd)
tensorLensLeft l1 l2 = reparam (tensor l1 l2) removeUnitLeft

||| update the left side across a depedent para lens
-- Given `ks` and `ls`:
--
--                                    p     q
--                                    ╷     ▲
--       ┌────────────┐            ┌──┴─────┴───┐
--   x ──┤            ├─> s    s ──┤            ├─> a
--       │     ks     │            │     ls     │
--   y <─┤            ├── t    t <─┤            ├── b
--       └────────────┘            └────────────┘
--
-- we obtain:
--
--                                 p     q
--                                 ╷     ▲
--       ┌─────────────────────────┴─────┴─────┐
--       │ ┌────────────┐       ┌──┴─────┴───┐ │
--   x ──┤─┤            ├─> s ──┤            ├─├─> a
--       │ │     ls     │       │     ks     │ │
--   y <─┤─┤            ├── t <─┤            ├─├── b
--       │ └────────────┘       └────────────┘ │
--       └─────────────────────────────────────┘
--
export
preCompose : DepLens s t x y -> DepParLens q p a b s t -> DepParLens q p a b x y
preCompose (MkDepLens g2 s2) (MkDepLens g1 s1) = MkDepLens
  (g1 . mapSnd (curry g2 ()))
  (\st, es, be => let r1 = s1 st (g2 (() && es)) be
                      r2 = s2 () es (p2 r1) in (p1 r1 && p2 r2))

||| update the right side across a dependent para lens
--  given `ls` and `ks`:
--
--          p     q
--          ╷     ▲
--       ┌──┴─────┴───┐            ┌────────────┐
--   s ──┤            ├─> a    a ──┤            ├─> x
--       │     ls     │            │     ks     │
--   t <─┤            ├── b    b <─┤            ├── y
--       └────────────┘            └────────────┘
--
--  we obtain:
--
--            p     q
--            ╷     ▲
--       ┌────┴─────┴──────────────────────────┐
--       │ ┌──┴─────┴───┐       ┌────────────┐ │
--   s ──┤─┤            ├─> a ──┤            ├─├─> x
--       │ │     ls     │       │     ks     │ │
--   t <─┤─┤            ├── b <─┤            ├─├─ y
--       │ └────────────┘       └────────────┘ │
--       └─────────────────────────────────────┘
--
export
postCompose : (ls : DepParLens p q a b s t) -> (ks : DepLens x y a b) -> DepParLens p q x y s t
postCompose (MkDepLens g1 s1) (MkDepLens g2 s2) = MkDepLens
    (g2 . (() &&) . g1)
    (\st, es, be => let r1 = s2 () (g1 (st && es)) be
                        r2 = s1 st es (p2 r1) in r2)

||| Tensor of lenses with a common parameter, requies an indexed monoid over `p` in order to combine the states
export
monoTensor : {prf : (v : p) -> Monoid (q v)} -> DepParLens p q a b s t
          -> DepParLens p q x y z w
          -> DepParLens p q (a * x) (\v => b v.fst * y v.snd) (s * z) (\v => t v.fst * w v.snd)
monoTensor v u = reparam (tensor v u) (MkDepLens (Product.dup . (.snd))
                                      (\(), x, y => () && let MkMonoid v = prf x in uncurry (<+>) y))

||| Extract and update the second element of a pair
export
fstLens : Lens a a (a * b) (a * b)
fstLens = MkLens p1 (\(a && b), a' => a' && b)

||| Extract and update the second element of a pair
export
sndLens : Lens b b (a * b) (a * b)
sndLens = MkLens p2 (\(a && b), b' => a && b')

||| The identity lens does nothing but carries along it arguments
public export
idLens : Lens a a a a
idLens = MkLens id const

||| Extract the parameter out and override it
--                   p     p
--                   ╷     ▲
--                ┌──┴─────┴───┐
--            x ──┤  └─────│───├─> p
--   p   ->       │        │   │
--            x <─┤        └───├── p
--                └────────────┘
--
export
stateLens : (0 p : Type) -> Para p p p p x x
stateLens _ = MkDepLens p1 (const (flip (&&)))

||| Extract the parameter out and override it
export
parameter : Para st st st st () ()
parameter = stateLens st
