module Isomorphism

%default total 

infix 6 ~=

public export
data (~=) : (a, b : Type) -> Type where
  MkIso : (to      : a -> b)
       -> (from    : b -> a)
       -> (0 from_to : (x : a) -> from (to x) === x)
       -> (0 to_from : (x : b) -> to (from x) === x)
       -> a ~= b

to : a ~= b -> a -> b

from : a ~= b -> b -> a

from_to : (iso : a ~= b) -> (x : a) -> from iso (to iso x) === x

to_from : (iso : a ~= b) -> (x : b) -> to iso (from iso x) === x

public export
reflIso : a ~= a
reflIso = MkIso id id (\x => Refl) (\x => Refl)

