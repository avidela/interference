module Lens

import Control.Monad.Identity

--Same Type Attack Bonus?
record Lens (s, t, a, b : Type) where
  constructor MkLens
  view : s -> a
  update : s -> b -> t

SLens : Type -> Type -> Type
SLens a b = Lens a a b b

record Address where
  constructor MkAddress
  line1 : String
  line2 : String
  postcode : String

record User where
  constructor MkUser
  uuid : String
  name : String
  age : Int
  address : Address


line1L : SLens Address String
line1L = MkLens line1 (\x, l => record {line1 = l} x)

line2L : SLens Address String
line2L = MkLens line2 (\x, l => record {line2 = l} x)

postcodeL : SLens Address String
postcodeL = MkLens postcode (\x, l => record {postcode = l} x)

nameL : SLens User String
nameL = MkLens name (\x, l => record {name = l} x)

ageL : SLens User Int
ageL = MkLens age (\x, l => record {age = l} x)

addressL : SLens User Address
addressL = MkLens address (\x, l => record {address = l} x)

composeLenses : Lens s t a b -> Lens a b p q -> Lens s t p q
composeLenses x y =
  MkLens (y.view . x.view)
         (\s, b => let v = y.update (x.view s) b in
                       x.update s v)

infixr 1 #->

data (#->) : Type -> Type -> Type where
  LFun : (1 f : (1 _ : a) -> b) -> a #-> b

interface LMonad m where
  pure : (1 _ : a) -> m a
  (>>=) : (1 _ : m a) -> (1 _ : (1 _ : a) -> m b) -> m b

lbind : LMonad m => (1 _ : m a) -> (1 _ : a #-> m b) -> m b
lbind ma (LFun f) = ma >>= f

LinearLens : (s, t, a, b : Type) -> (m1, m2 : Type -> Type) -> Type
LinearLens s t a b m1 m2 =
  s #-> m1 (a , (b #-> m2 t))


infixl 2 |>
infixl 2 `•`

(|>) : (1 _ : (a #-> b)) -> (1 _ : a) -> b
(|>) (LFun f) a = f a

• : (a #-> b) #-> (b #-> c) #-> (a #-> c)
• = LFun \f => LFun \g => LFun \a => g |> (f |> a)


lcomp : LMonad m  => (a #-> m b)
                 #-> (b #-> m c)
                 #->  a #-> m c
lcomp = LFun \f => LFun \g => LFun \a => do f |> a `lbind` g

composeLLens : LMonad m1
            => LMonad m2
            => LinearLens s t a b m1 m2
           #-> LinearLens a b p q m1 m2
           #-> LinearLens s t p q m1 m2
composeLLens = LFun \(LFun l1) => LFun \(LFun l2) => LFun \s =>
  do (a', f) <- l1 s
     (p', g) <- l2 a'
     Lens.pure $ MkPair p' (lcomp |> g |> f)

NormalLens : (s, t, a, b : Type) -> Type
NormalLens s t a b = LinearLens s t a b Identity Identity

infix 3 ~=

record (~=) (a, b : Type) where
  constructor MkIso
  to : a -> b
  from : b -> a
  fromTo : {x : a} -> from (to x) = x
  toFrom : {x : b} -> to (from x) = x

lensIsomorphism : NormalLens s t a b ~= Lens s t a b
lensIsomorphism = MkIso
  (\(LFun l) => MkLens (\s => fst $ runIdentity $ l s)
                       (\s, b => runIdentity ((snd $ runIdentity $ l s) |> b)))
  (\(MkLens view update) =>
      (LFun \1 s => pure (?fail_because_view_non_linear {-view s-}
                         , LFun \b => pure ?fail_bc_s_used_and_b_linear {-update s b-} )))
  ?tofrabbb
  ?lensIsomorphism_rhs

-- Argument has to be non-linear in order to allow using the lens twice, once for each function
fromLinear : (LinearLens s t a b Identity Identity) -> Lens s t a b
fromLinear (LFun l) = MkLens (\s => fst $ runIdentity $ l s)
                             (\s, b => runIdentity ((snd $ runIdentity $ l s) |> b))


